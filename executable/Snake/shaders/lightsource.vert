#version 330

layout (location = 0) in vec4 position;

uniform mat4 transformMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main()
{	
	gl_Position = transformMatrix * viewMatrix * modelMatrix * position;
}
