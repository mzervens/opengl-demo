#version 330

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 surfaceNormal;

uniform mat4 transformMatrix;
uniform mat4 viewMatrix;
uniform mat4 lightPV;

uniform mat4 modelMatrix;
uniform mat3 modelMatForNormal;

out vec3 normal;
out vec3 fragPosition;

out vec2 textureCoord;

out vec4 fragmentFromLightPOV;

void main()
{	
	gl_Position = transformMatrix * viewMatrix * modelMatrix * position;
	
	textureCoord = texCoord;
	normal = modelMatForNormal * surfaceNormal;
	fragPosition = position.xyz;

	fragmentFromLightPOV = lightPV * modelMatrix * position;
}
