#version 330

out vec4 outputColor;

uniform sampler2D textureData;
uniform sampler2D shadowmap;

uniform vec3 lightPosition;
uniform vec3 lightDirection;
uniform vec3 lightColor;

uniform int lightType;

in vec2 textureCoord;
in vec3 normal;
in vec3 fragPosition;
in vec4 fragmentFromLightPOV;

uniform mat3 viewNormalMatrix;
uniform vec3 cameraPosition;
uniform float ambientStrength;

float isInShadow(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

	// Converts from -1 to 1 ==> 0 to 1
	projCoords = projCoords * 0.5 + 0.5; 

	// Gets the depth value stored in shadowmap which corresponds to the closest object z value from the lights pov
	float closestDepth = texture(shadowmap, projCoords.xy).x;

	float bias = 0.0;
	float currFragmentsDepth = projCoords.z - bias;
	if(currFragmentsDepth > 1.0)
	{
		return 1.0;
	}
	
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(shadowmap, 0);


	if(currFragmentsDepth > closestDepth)
	{
		return 0.0;
	}
	else
	{
		return 1.0;
	}

/*
	for(int x = -1; x <= 2; ++x)
	{
		for(int y = -1; y <= 2; ++y)
		{
			float pcfDepth = texture(shadowmap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currFragmentsDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= 16.0;
	shadow = 1.0 - shadow;
	return shadow;
	*/
}


void main()
{
	vec3 surfaceToLight;

	if(lightType == 1)
	{
		surfaceToLight = lightPosition - fragPosition;
	}
	else
	{
		surfaceToLight = normalize(-lightDirection);
	}

	vec3 worldNormal = normal;	// If the object had been rotated by model transformation then the normal should be rotated too
	vec3 lightToSurface = -surfaceToLight;
	vec3 surfaceToCamera = normalize(cameraPosition - fragPosition);

	vec3 reflectionVector = reflect(lightToSurface, worldNormal);

	float specularCoefficient = max(0.0, dot(reflectionVector, surfaceToCamera));
	specularCoefficient = pow(specularCoefficient, 1); // 96 is the material shininess value should be taken from the mtl file at index Ns

	vec3 specularLight = specularCoefficient * vec3(0.001, 0.001, 0.001) * lightColor; // middle vector represents the materials reflected specular color which should be taken from the mtl file at index Ks

	float distanceToLight = length(surfaceToLight);
	float attenuation = 1.0 / (1.0 + 0.001 * pow(distanceToLight, 2)); // constant multiplied by d^2 is the attenuation constant which modifies how fast the light loses its strength
	

	// cos(angle between surface to light vector and surface normal)
	float diffuseCoefficient = dot(worldNormal, surfaceToLight) / (length(worldNormal) * distanceToLight);
	
	// Since brightness can be negative if the light is behind the object
	diffuseCoefficient = max(0.0, diffuseCoefficient);


	outputColor = texture(textureData, textureCoord);

	vec3 ambientLight = vec3((outputColor.rgb * lightColor) * ambientStrength);
	vec3 diffuseLight = vec3((outputColor.rgb * lightColor) * diffuseCoefficient);


	float shadow = isInShadow(fragmentFromLightPOV); 
	//float shadow = 1.0;

	vec3 attenuatedDiffuseSpecLight = attenuation * (diffuseLight + specularLight);

	outputColor = vec4((attenuatedDiffuseSpecLight * shadow) + ambientLight, outputColor.a);


	// Gamma correction
	vec3 gamma = vec3(1.0/2.2);
	outputColor = vec4(pow(vec3(outputColor), gamma), outputColor.a);
	
}