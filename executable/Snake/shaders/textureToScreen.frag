#version 330

in vec2 textureCoordinate;
out vec4 outputColor;

uniform sampler2D textureData;

void main()
{
	//outputColor = texture(textureData, textureCoordinate);
	float depth = texture(textureData, textureCoordinate).x;
	outputColor = vec4(vec3(depth), 1.0);
}