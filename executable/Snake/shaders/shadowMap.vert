#version 330

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 surfaceNormal;

uniform mat4 PV;
uniform mat4 modelMatrix;

void main()
{	

	gl_Position = PV * modelMatrix * position;
}